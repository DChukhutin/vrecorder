
#include <stdint.h>
#include "stm32l476xx.h"

#define HSE_FREQ		(12000000ULL)
#define	PLLM				(1UL)
#define	PLLN				(80UL)
#define PLLR				(1UL)

static volatile uint32_t counter;

int clock_Init(void)
{
	// Reset Clock domain to defaul state
	
  RCC->CR |= RCC_CR_MSION; 			/* Reset the RCC clock configuration to the default reset state */  
  RCC->CFGR = 0x00000000U;			/* Reset CFGR register */
  RCC->CR &= 0xEAF6FFFFU;				/* Reset HSEON, CSSON , HSION, and PLLON bits */
  RCC->PLLCFGR = 0x00001000U;		/* Reset PLLCFGR register */
  RCC->CR &= 0xFFFBFFFFU;				/* Reset HSEBYP bit */
  RCC->CIER = 0x00000000U;			/* Disable all interrupts */
	
	// Enable HSE oscillator 
	RCC->CR |= RCC_CR_HSEON;
	// wait while HSE started
	counter = 4000000ULL;
	while(((RCC->CR & RCC_CR_HSERDY) == 0) && --counter);
	if(counter == 0) return 1; // HSE not started
	
	// Enable and configure PLL
	RCC->PLLCFGR = (PLLM << RCC_PLLCFGR_PLLM_Pos) |
								 (PLLN << RCC_PLLCFGR_PLLN_Pos) |
								 (PLLR << RCC_PLLCFGR_PLLR_Pos) |
								 (RCC_PLLCFGR_PLLREN) |
								 (RCC_PLLCFGR_PLLSRC_HSE);			/* PLL SRC -> HSE */
	// PLL on
	RCC->CR |= RCC_CR_PLLON;
	// wait while PLL started
	counter = 4000000ULL;
	while(((RCC->CR & RCC_CR_PLLRDY) == 0) && --counter);
	if(counter == 0) return 1; // PLL not started
	
	// Set Flash lateny
	FLASH->ACR &= ~(FLASH_ACR_LATENCY_Msk); // reset
	FLASH->ACR |=  (FLASH_ACR_LATENCY_4WS);
	
	// Set AHB, APB1 and APB2 clock prescaler
	RCC->CFGR = RCC_CFGR_PPRE1_DIV2 |
							RCC_CFGR_PPRE2_DIV2 |
							RCC_CFGR_HPRE_DIV1;
							
	// Switch system clock to PLL
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	// wait while system clock switched
	counter = 4000000ULL;
	while(((RCC->CFGR & RCC_CFGR_SWS_PLL) != RCC_CFGR_SWS_PLL) && --counter);
	if(counter == 0) return 1; // system clock  not switched	
	
	return 0;
	
}

//------------- End of File ---------------------


